import React from 'react';
import Audio from './audio';

class Greeter extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      isAudioAvailable: true,
      toggleAudio: this.toggleAudio.bind(this)
    };
  }

  toggleAudio() {
    this.setState({isAudioAvailable: !this.state.isAudioAvailable});
  }

  render() {
    return (
        <div>
          <h1>Hello!</h1>
          <Audio />
        </div>
      )
  }

  getChildContext() {
    return this.state;
  }
}

Greeter.childContextTypes = {
  isAudioAvailable: React.PropTypes.bool.isRequired,
  toggleAudio: React.PropTypes.func.isRequired
};

export default Greeter;
