import React from 'react';
import Equalizer from './equalizer';

class Audio extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
        <div>
          {(this.context.isAudioAvailable
            ? <Equalizer />
            : false
          )}
          <p>Audio controls <span>{this.context.isAudioAvailable ? 'On' : 'Off'}</span></p>
          <button onClick={this.context.toggleAudio}>Toggle audio</button>
        </div>
      )
  }
}

Audio.contextTypes = {
  isAudioAvailable: React.PropTypes.bool.isRequired,
  toggleAudio: React.PropTypes.func.isRequired
};

export default Audio;
