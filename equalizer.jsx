import React from 'react';

class Equalizer extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
        <div>
          <pre>
            === Equalizer ===<br />
            | o | | o | | o |<br />
            o | | o | | o | |<br />
            | | o | | o | | o<br />
          </pre>
        </div>
      )
  }
}

export default Equalizer;
